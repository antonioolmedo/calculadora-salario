#include <iostream>
#include <string>
#include "Calculator.h"

using namespace std;

bool askToCalcAgain();

int main()
{
	Calculator* c;
	do {
		c = new Calculator();
	} while (askToCalcAgain());
	return 0;
}

bool askToCalcAgain()
{
	cout << endl << endl << "¿Quieres volver a calcular? (s/n)";
	string response = "";
	getline(cin, response);
	cout << endl << endl;
	return  response[0] == 's' || response[0] == 'S';
}

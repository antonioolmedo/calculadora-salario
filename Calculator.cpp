#include "Calculator.h"	

Calculator::Calculator()
{
	SetConsoleOutputCP(1252);
	SetConsoleCP(1252);
	cout << "Bienvenido a la calculadora de salario." << endl;

	askSocialSecurityPercent();
	askYearlyGrossSalary();
	askPaymentCount();

	monthlyGrossSalary = (float)yearlyGrossSalary / (float)paymentCount;
	cout << "	Tu salario bruto mensual es de: " << monthlyGrossSalary << " Euros" << endl;

	yearlySocialSecurity = (float)yearlyGrossSalary * (float)socialSecurityPercent / 100;
	cout << "	Tu aporte anual a la seguridad social es de: " << yearlySocialSecurity << " Euros" << endl;

	askIRPF();
	yearlyIRPF = (float)yearlyGrossSalary * (float)irpfPercent / 100;
	cout << "	Tu aporte anual al IRPF es de: " << yearlyIRPF << endl;

	monthlyNetSalary = ((float)yearlyGrossSalary - yearlySocialSecurity - yearlyIRPF) / paymentCount;
	cout << "	Tu salario neto mensual ser� de: " << monthlyNetSalary << " Euros" << endl;
}

void Calculator::askSocialSecurityPercent()
{
	do {
		cout << "Introduce el porcentaje de la seguridad social: ";
		socialSecurityPercent = getFloatFromConsole();
	} while (socialSecurityPercent <= 0);
}

void Calculator::askPaymentCount()
{
	do {
		cout << "Introduce el n�mero de pagas: ";
		paymentCount = getIntFromConsole();
	} while (paymentCount <= 0);
}

void Calculator::askYearlyGrossSalary()
{
	do {
		cout << "Introduce el salario bruto anual: ";
		yearlyGrossSalary = getIntFromConsole();
	} while (yearlyGrossSalary <= 0);
}

void Calculator::askIRPF()
{
	do {
		cout << "Introduce el IRPF: ";
		irpfPercent = getFloatFromConsole();
	} while (irpfPercent <= 0);
}

int Calculator::getIntFromConsole()
{
	string response = "";
	try
	{
		getline(cin, response);
		return stoi(response);
	}
	catch (invalid_argument ex) {
		return 0;
	}
}

float Calculator::getFloatFromConsole()
{
	string response = "";
	try
	{
		getline(cin, response);
		return stof(response);
	}
	catch (invalid_argument ex) {
		return 0.00;
	}
}
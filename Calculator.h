#pragma once
#include <iostream>
#include <windows.h>
#include <string>
#include <stdexcept>

using namespace std;

class Calculator {
public:
	Calculator();
	static int getIntFromConsole();
	static float getFloatFromConsole();
private:
	int paymentCount = 0;
	int yearlyGrossSalary = 0;
	float monthlyGrossSalary = 0.00;
	float monthlyNetSalary = 0.00;
	float yearlySocialSecurity = 0.00;
	float irpfPercent = 0.00;
	float yearlyIRPF = 0.00;
	float socialSecurityPercent = 0.00;

	void askSocialSecurityPercent();
	void askPaymentCount();
	void askYearlyGrossSalary();
	void askIRPF();
};